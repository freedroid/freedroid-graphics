# ***** BEGIN GPL LICENSE BLOCK *****
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****

bl_info = {
    "name": "Freedroid Tools",
    "author": "basse",
    "version": (0, 4),
    "blender": (2, 69, 10),
    "location": "Toolshelf > Freedroid Tools",
    "description": "Tools for Freedroid assets rendering",
    "warning": "",
    "category": "Render" }


from subprocess import call
import bpy, os
import sys
import mathutils
from math import *
from bpy.types import Panel, Menu, Operator, Scene, PropertyGroup
from bpy.props import BoolProperty, StringProperty, PointerProperty, IntProperty
from bpy.app.handlers import persistent

EDIT_MODES = {'EDIT_MESH', 'EDIT_CURVE', 'EDIT_SURFACE', 'EDIT_METABALL', 'EDIT_TEXT', 'EDIT_ARMATURE'}

# --------------------------------------------------------------------------------
#               run croppy command for image that was just rendered
# --------------------------------------------------------------------------------
@persistent
def runCroppy(scene):
    fdts = scene.freedroid_tools_settings
    if fdts.run_croppy:
        if fdts.target_basename:

            rndr = scene.render
            format = rndr.image_settings.file_format


            frameExt = scene.render.image_settings.file_format.lower()
            framePath = scene.render.frame_path()

            filePath = scene.render.filepath

            #print("framePath: %s" % framePath)
            #print("filePath: %s" % filePath)

            targetDir = os.path.dirname(fdts.target_basename)

            # let's check if the destination directory exists, if not, we create it

            print("Checking target directory (%s) ... " % (targetDir), end="")
            if not os.path.exists(targetDir):
                print("CREATED.")
                os.makedirs(targetDir)
            else:
                print("OK.")


            if (framePath.count('enemy_rot') > 0):
                # if we are rendering droid action animations
                # we need to set the filename and paddings a bit differently for croppy to work
                paddings = 2
                framePath = filePath + '.png'
            else:
                # otherwise let check and use whatever paddings are set (and default to 4)
                paddings = scene.render.filepath.count('#')
                if paddings == 0:
                    paddings = 4


            if filePath[-4:].lower() in ['.png', '.jpg']:
                # if there is extension in output path we assume this is single frame rendering
                # and not animation, so lets save the file for croppy (blender doesnt do this automaticly)

                image = bpy.data.images['Render Result']
                if not image:
                    print('Error: Render Result not found. Image not saved')
                    return

                print('Saved image:', filePath)
                image.save_render(filePath, scene=None)

                framePath = filePath
                targetPath = "%s.%s" % (fdts.target_basename, frameExt)

                # we don't need this since it's single frame
                frameNumberPadded = ""
            else:
                # it's animation frame that was rendered...
                # lets add frame number and zero pad it
                frameNumberPadded = str(scene.frame_current).zfill(paddings)

                # path and filename for croppy output file
                targetPath = "%s%s.%s" % (fdts.target_basename, frameNumberPadded, frameExt)

            # lets see if there is offset empty
            # if not, we fallback to croppy calculations
            if bpy.data.objects.get('offset'):
                if bpy.data.objects.get('iso_camera') is None:
                    #info('Error!*you have offset empty set, but correct camera is missing. please add correct camera or remove offset empty.')
                    print('Error! you have offset empty set, but correct camera is missing. please add correct camera or remove offset empty.')
                    return -1
                else:
                  # generate offset files
                  print("Write offset file: %s%s.%s" % (fdts.target_basename, frameNumberPadded, 'offset'))
                  write_offset(scene, "%s%s.%s" % (fdts.target_basename, frameNumberPadded, 'offset'))
            else:
                print("No offset empty found, croppy will calculate offset files!")


            # run croppy
            croppyCmd = "croppy -i %s -o %s" % (framePath, targetPath)
            #croppyCmd = "croppy -i %s%s.png -o %s" % (frameName.replace('##', ''), frameNumberPadded, targetPath)
            print("Running: " + croppyCmd)
            os.system(croppyCmd)

        else:
            #info(scene, "oh no!*You are missing target basename!")
            print("oh no! You are missing target basename!")
            return

# --------------------------------------------------------------------------------
# Orthographic camera automatic rotation functions
# used when rendering out directions for droid/npc animations
# --------------------------------------------------------------------------------

def get_output_direction():
    '''
    Return which direction the camera is at, as a number
    between 0 and N_DIRECTIONS-1 (inclusive).
    '''

    our_cam = bpy.data.objects['iso_camera']
    N_DIRECTIONS=8
    x, y = our_cam.location.x, our_cam.location.y
    # Protect against division by zero by losing unimportant accuracy.
    if abs(x) < 0.0001:
        x = 0.0001
    if x > 0:
        if y > 0:
            a = atan(y/x)
        else:
            a = 2*pi - atan(-y/x)
    else:
        if y > 0:
            a = pi - atan(y/-x)
        else:
            a = pi + atan(-y/-x)
    #print('angle (degrees) =', int(a * 180 / pi))
    a += pi / N_DIRECTIONS
    return int(a / (2*pi / N_DIRECTIONS)) % N_DIRECTIONS

def reset_cam():
    '''
    Reset camera to original location
    '''
    #our_cam.location = (-4.0, 0.0, 2.5)
    our_cam = bpy.data.objects['iso_camera']

    our_cam.location = (0.0, -4.0, 2.5)
    #bpy.ops.object.camera_add(location=(-4, 0, 2.5), rotation=(1.04719, 0.0, -248.971710))

def do_cam_rot():
    '''
    Move the camera to the next of N_DIRECTIONS positions along
    a circle of radius 4.00 with constant Z.
    '''
    N_DIRECTIONS = 8
    our_cam = bpy.data.objects['iso_camera']
    R = 4.00
    da = 2*pi / N_DIRECTIONS
    n_step = get_output_direction() - 1
    X_new = R * cos(da * n_step)
    Y_new = R * sin(da * n_step)

    our_cam.location.x = X_new
    our_cam.location.y = Y_new


# --------------------------------------------------------------------------------
#                       offset computation functions
# --------------------------------------------------------------------------------
def compute_default_frustum(near, far, lens, aspect_ratio):
    half_size = 16 * near / lens
    if aspect_ratio > 1:
        size_x = half_size
        size_y = half_size / aspect_ratio
    else:
        size_x = half_size * aspect_ratio
        size_y = half_size
    return -size_x, size_x, -size_y, size_y, near, far

def compute_default_ortho(near, far, scale, aspect_ratio):
    half_size = scale / 2
    if aspect_ratio > 1:
        size_x = half_size
        size_y = half_size / aspect_ratio
    else:
        size_x = half_size * aspect_ratio
        size_y = half_size
    return -size_x, size_x, -size_y, size_y, -far, near

def get_frustum_matrix(left, right, bottom, top, near, far):
    return mathutils.Matrix([
        [2.0 * near / (right - left), 0.0, (right + left) / (right - left), 0.0],
        [0.0, 2.0 * near / (top - bottom), (top + bottom) / (top - bottom), 0.0],
        [0.0, 0.0, - (far + near) / (far - near), - (2 * far * near) / (far - near)],
        [0.0, 0.0, -1.0, 0.0]])

def get_ortho_matrix(left, right, bottom, top, near, far):
    return mathutils.Matrix([
        [2.0 / (right - left), 0.0, 0.0, - (right + left) / (right - left)],
        [0.0, 2.0 / (top - bottom), 0.0, - (top + bottom) / (top - bottom)],
        [0.0, 0.0, -2.0 / (far - near), -(far + near) / (far - near)],
        [0.0, 0.0, 0.0, 1.0]])

def get_perspective_matrix(camera, aspect_ratio):
    if camera.type == "ORTHO":
        left, right, bottom, top, near, far = compute_default_ortho(camera.clip_start, camera.clip_end, camera.ortho_scale, aspect_ratio)
        return get_ortho_matrix(left, right, bottom, top, near, far)
    elif camera.type == "PERSP":
        left, right, bottom, top, near, far = compute_default_frustum(camera.clip_start, camera.clip_end, camera.lens, aspect_ratio)
        return get_frustum_matrix(left, right, bottom, top, near, far)

def write_offset(scene, filename):
    res_x = scene.render.resolution_x * scene.render.resolution_percentage / 100
    res_y = scene.render.resolution_y * scene.render.resolution_percentage / 100
    pix_asp_x = scene.render.pixel_aspect_x
    pix_asp_y = scene.render.pixel_aspect_y

    aspect_ratio = 1.0
    if res_y != 0:
        aspect_ratio = (res_x * pix_asp_x) / (res_y * pix_asp_y)

    camera = bpy.data.objects["iso_camera"]
    camera_mat = camera.matrix_world.copy()
    camera_mat.invert()

    camera = camera.data
    persp_mat = get_perspective_matrix(camera, aspect_ratio)

    offset_object = bpy.data.objects["offset"]
    offset_pos = offset_object.location.copy()
    offset_point = persp_mat * (camera_mat * offset_pos)
    xnd = offset_point.x
    ynd = offset_point.y
    center_x, center_y = (xnd + 1) * (res_x / 2.0), res_y - (ynd + 1) * (res_y / 2.0)
    offset_x, offset_y = -round(center_x), -round(center_y)

    with open(filename, 'w') as offset_file:
        offset_file.write("OffsetX=" + str(offset_x) + "\n")
        offset_file.write("OffsetY=" + str(offset_y) + "\n")


# --------------------------------------------------------------------------------
#               preset settings for lamps, cameras etc..
# --------------------------------------------------------------------------------


def createIsoLamps(classic = True):
    if (classic):
        if bpy.data.objects.get('iso_spot_lamp') is None:
            bpy.ops.object.add(type='LAMP', location=(3.0, -4.0, 9.0), rotation=(0.547990, -0.22343, 0.900148))

            ob = bpy.context.active_object
            ob.name = 'iso_spot_lamp'
            ob.scale = (0.289, 0.289, 0.289)

            lamp = ob.data
            lamp.name = 'Lamp' + ob.name
            lamp.type = 'SPOT'
            lamp.energy = 2.170
            lamp.shadow_buffer_clip_start = 6.200
            lamp.shadow_buffer_size = 1024
            lamp.falloff_type = 'INVERSE_LINEAR'
        else:
            info("Oh wait!*there seems to be already spot setup..")
    else:
        if bpy.data.objects.get('iso_sun_lamp') is None:
            bpy.ops.object.add(type='LAMP', location=(2.59532, -4.08520, 9.0), rotation=(0.45664, -0.18743, 0.92137))

            ob = bpy.context.active_object
            ob.name = 'iso_sun_lamp'

            lamp = ob.data
            lamp.name = 'Lamp' + ob.name
            lamp.type = 'SUN'
            lamp.energy = 0.952
            lamp.shadow_method = 'RAY_SHADOW'
            lamp.shadow_soft_size = 66.9216
            lamp.shadow_ray_samples = 24
        else:
            info("Oh wait!*looks like there is already sun setup ...")

    # lets add hemi lamp for fill light

    if bpy.data.objects.get('iso_hemi_lamp') is None:
        bpy.ops.object.add(type='LAMP', location=(-4.0, -4.0, 7.0), rotation=(0.78539, 0.0, -0.78539))
        ob = bpy.context.active_object
        ob.name = 'iso_hemi_lamp'
        lamp = ob.data
        lamp.name = 'Lamp' + ob.name
        lamp.type = 'HEMI'
        lamp.energy = 0.470
    else:
        info("Oh wait!*looks like there is already hemi setup ...")

#
# creates shadow plane
#
def createShadowPlane():
    if bpy.data.objects.get('iso_shadow_plane') is None:
        bpy.ops.mesh.primitive_plane_add(location=(0.0, 0.0, 0.0))
        ob = bpy.context.active_object
        ob.name = 'iso_shadow_plane'

        try:
            mat = bpy.data.materials['iso shadow plane']
            info("Old material found*looks like you had shadow only material,*lets use that then..")
        except:
            mat = bpy.data.materials.new('iso shadow plane')
            mat.use_transparency = True
            mat.alpha = 0.705
            mat.use_only_shadow = True

        ob.data.materials.append(mat)
    else:
        info("Oh wait!*looks like you already have shadow plane")

#
# create roto camera and focus point for it
#
def createRotoCamera():
    # lets create the camera focus point
    if bpy.data.objects.get('roto_camera_focus') is None:
        bpy.ops.object.empty_add(location=(0, 0, 0.27))
        ob = bpy.context.active_object
        ob.name = 'roto_camera_focus'

    # lets create actual camera and set focus track
    if bpy.data.objects.get('iso_camera') is None:
        #bpy.ops.object.camera_add(location=(-4, 0, 2.5), rotation=(1.04719, 0.0, -248.971710))
        bpy.ops.object.camera_add(location=(-2.828427, -2.828427, 4.0), rotation=(1.04719, 0.0, -0.78539))
        ob = bpy.context.active_object
        ob.name = 'iso_camera'
        cam = ob.data
        cam.name = 'Camera' + ob.name
        cam.type = 'ORTHO'
        cam.ortho_scale = 5.0
        cam.sensor_width = 32.0
        cam.draw_size = 0.50
        cam.show_passepartout = True
        cam.passepartout_alpha = 0.666
        bpy.context.scene.camera = ob
        bpy.ops.object.constraint_add(type='TRACK_TO')
        con = ob.constraints.active
        con.name = 'camera focus'
        con.target = bpy.data.objects.get('roto_camera_focus')
        con.up_axis = 'UP_Y'
        con.track_axis = 'TRACK_NEGATIVE_Z'
    else:
        info("Oh wait!*looks like you already have roto cam")

     # and finally create the lamps and parent to camera
    createIsoLamps()

    c = bpy.data.objects.get('iso_camera')
    bpy.ops.object.select_pattern(pattern="iso_camera", extend=False)
    bpy.ops.object.select_pattern(pattern="iso_hemi_lamp")
    bpy.ops.object.select_pattern(pattern="iso_spot_lamp")
    bpy.context.scene.objects.active = c
    bpy.ops.object.parent_set(keep_transform=True)

#
# creates isometric camera correctly positioned
#
def createIsoCamera():
    if bpy.data.objects.get('iso_camera') is None:
        bpy.ops.object.camera_add(location=(-2.828427, -2.828427, 4.0), rotation=(1.04719, 0.0, -0.78539))
        ob = bpy.context.active_object
        ob.name = 'iso_camera'
        cam = ob.data
        cam.name = 'Camera' + ob.name
        cam.type = 'ORTHO'
        cam.ortho_scale = 5.0
        cam.sensor_width = 32.0
        cam.draw_size = 0.50
        cam.show_passepartout = True
        cam.passepartout_alpha = 0.666
        bpy.context.scene.camera = ob
    else:
        info("Oh wait!*looks like you already have ortho cam")

#
# sets isometric resolution, image formats and transparency
#
def setIsoRenderSettings():
    rndr = bpy.context.scene.render

    rndr.resolution_x = 200
    rndr.resolution_y = 240
    rndr.resolution_percentage = 100
    rndr.alpha_mode = 'TRANSPARENT'
    rndr.use_raytrace = True
    rndr.image_settings.file_format = 'PNG'
    rndr.image_settings.color_mode = 'RGBA'

#
# adds offset empty in middle of scene
#
def createOffsetEmpty():
    if bpy.data.objects.get('offset') is None:
        bpy.ops.object.add(type='EMPTY', location=(0.0, 0.0, 0.0), rotation=(1.5708, -0.0, 0.0))
        ob = bpy.context.active_object
        ob.name = 'offset'
        ob.empty_draw_type = 'CIRCLE'
        ob.empty_draw_size = 3.0
    else:
        info("Oh wait!*there seems to be already an offset marker..*or atleast object with that name")


# --------------------------------------------------------------------------------
#                               UI STUFF
# --------------------------------------------------------------------------------


#
# creates group for freedroidtool settings
#
class freedroidToolsSettings(PropertyGroup):
    #target = bpy.context.scene.render.filepath
    run_croppy = BoolProperty(
        name = 'Run croppy',
        default = False,
        description='Run croppy after rendering a frame')

    target_basename = StringProperty(
        name = 'Target dir/basename',
        subtype = 'FILE_PATH',
        default = "",
        description='Target filename of frames')

    target_armature = StringProperty(
        name = 'Armature',
        default = "",
        description='Name of armature that has the actions')

    number_of_directions = IntProperty(
        name = 'Number of direction angles',
        default = 8,
        description = 'Number of how many camera rotations we are rendering out')

    status = StringProperty(
        name = 'Tool status',
        default = "Render droid",
        description='Status bar for little information')


@persistent
def buildArmatureList(context):

    objects = bpy.data.objects

    if objects.is_updated:
        context.armatureList.clear()

        for ob in objects:
            if ob.type == "ARMATURE":
                context.armatureList.add().name = ob.name

        #print("We built armatureList, and it was fine...")

#
# UI goes to "T" - toolshelf
#
class freedroidToolsPanel(Panel):
    bl_idname = "fdt.panel"
    bl_label = "Freedroid Tools"
    bl_region_type = "TOOLS"
    bl_space_type = "VIEW_3D"
    bl_category = "Freedroid"
    #bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(self, context):
        return ((getattr(context, "mode", 'EDIT_MESH') not in EDIT_MODES) and
                (context.area.spaces.active.type == 'VIEW_3D'))

    #bl_space_type = 'PROPERTIES'
    #bl_region_type = 'WINDOW'
    #bl_context = 'render'

    def draw(self, context):
        scn = context.scene
        layout = self.layout
        fdts = scn.freedroid_tools_settings

        #layout.operator("mesh.primitive_plane_add", text="Plane", icon='MESH_PLANE')




        row = layout.row(align = True)
        row.label(text="Create:")

        row = layout.row(align = True)
        row.operator("fdt.create_offset_empty", "Offset empty", icon='EMPTY_DATA')
        row = layout.row(align = True)
        row.operator("fdt.create_iso_cam", "Ortho Camera", icon="CAMERA_DATA")
        row = layout.row(align = True)
        row.operator("fdt.create_iso_lamps_classic", "Iso lamps (classic)", icon="LAMP_DATA")
        row = layout.row(align = True)
        row.operator("fdt.create_iso_lamps", "Iso lamps", icon="LAMP_DATA")
        row = layout.row(align = True)
        row.operator("fdt.create_shadow_plane", "Shadow plane", icon="MESH_PLANE")


        row = layout.row(align = True)
        row.label(text="Presets:")

        row = layout.row(align = True)
        row.operator("fdt.set_iso_render_settings", "Set render options", icon="RENDER_RESULT")


        row = layout.row(align = True)
        row.prop(fdts, "run_croppy")
        row = layout.row(align = True)
        row.prop(fdts, "target_basename")
        row = layout.row(align = True)
        row.operator("fdt.get_from_output")

        row = layout.row(align = True)
        row.label(text="Droid rendering:")

        row = layout.row(align = True)
        # row.prop(fdts, "target_armature")
        row.prop_search(fdts, "target_armature", scn, "armatureList", icon='ARMATURE_DATA')
        row = layout.row(align = True)
        row.prop(fdts, "number_of_directions")


        # row = layout.row(align = True)
        # row.label(text="Render actions:")
        for action in bpy.data.actions:
            row = layout.row(align = True)
            row.prop(action, 'render', text=action.name)

        row = layout.row(align = True)
        row.operator("fdt.render_droid", text=fdts.status, icon="RENDER_STILL")


#
# operator for copying render output field information into croppy target basename field
#
class fdt_getTargetFromOutput(Operator):
    bl_idname = "fdt.get_from_output"
    bl_label = "Get path from render output"
    bl_description = "Get target directory from render output field"

    def execute(self, context):
        scn = context.scene
        fdts = scn.freedroid_tools_settings
        rndr = scn.render

        fdts.target_basename = rndr.filepath

        return {'FINISHED'}

#
# Operatorsfor iso -presets
#
class fdt_createIsoCam(Operator):
    bl_idname = "fdt.create_iso_cam"
    bl_label = "Create orthographic camera"
    bl_description = "Creates ortho camera and places it correctly"

    def execute(self, context):
        createIsoCamera()
        return {'FINISHED'}

class fdt_createRotoCam(Operator):
    bl_idname = "fdt.create_roto_cam"
    bl_label = "Create orthographic roto camera"
    bl_description = "Creates ortho camera for droid rotations (old roto_cam)"

    def execute(self, context):
        createRotoCamera()
        return {'FINISHED'}

class fdt_createIsoLampsClassic(Operator):
    bl_idname = "fdt.create_iso_lamps_classic"
    bl_label = "Create SPOT+HEMI lamp setup"
    bl_description = "This is the current setup that is quite nice and fast.. but not good for seamless tiles."

    def execute(self, context):
        createIsoLamps(True)
        return {'FINISHED'}

class fdt_createIsoLamps(Operator):
    bl_idname = "fdt.create_iso_lamps"
    bl_label = "Create SUN+HEMI lamp setup (EXPERIMENTAL)"
    bl_description = "NOTE: This is still wip and options will change!! It will be better for seamless tiling."

    def execute(self, context):
        createIsoLamps(False)
        return {'FINISHED'}

class fdt_createShadowPlane(Operator):
    bl_idname = "fdt.create_shadow_plane"
    bl_label = "Create only-shadow plane (EXPERIMENTAL)"
    bl_description = "NOTE: New system for overlay floor tiles to get shadow.. still WIP.."

    def execute(self, context):
        createShadowPlane()
        return {'FINISHED'}

class fdt_createOffsetEmpty(Operator):
    bl_idname = "fdt.create_offset_empty"
    bl_label = "Create offset empty for croppy"
    bl_description = "We are no longer using croppy to produce offsets. This empty is needed for calculations."

    def execute(self, context):
        createOffsetEmpty()
        return {'FINISHED'}

class fdt_setIsoRenderSettings(Operator):
    bl_idname = "fdt.set_iso_render_settings"
    bl_label = "Set rendering options"
    bl_description = ""

    def execute(self, context):
        setIsoRenderSettings()
        return {'FINISHED'}


class fdt_renderAction(Operator):
    bl_idname = "fdt.render_action"
    bl_label = "Render action"

    _timer = None

    _frameN = 0
    _frameStart = 0
    _frameEnd = 0
    _actionN = 0
    _action = None

    #directions = bpy.props.IntProperty()
    _direction = 0
    _rendering = False

    renderList = []

    #def do_render(self):
        #bpy.ops.render.render(animation=True)
        #bpy.context.scene.frame_set(_frameN)
        #bpy.ops.render.render(write_still=True)

    def loadAction(self, n):
        print("Loaded action for rendering: %s" % (n))
        fdts = bpy.context.scene.freedroid_tools_settings
        arm = bpy.data.objects[fdts.target_armature]
        act = self.renderList[n]
        arm.animation_data.action = act # set action

        reset_cam()

        self._direction = 0

        self._action = act.name
        self._frameStart = act.frame_range.x
        self._frameEnd = act.frame_range.y
        self._frameN = self._frameStart



    def modal(self, context, event):
        if event.type in { 'ESC' }:
            return self.cancel(context)

        if event.type == 'TIMER':
            fdts = context.scene.freedroid_tools_settings
            #if self._frameN == self._frameStart:
            fdts.target_basename = '/tmp/finfile/enemy_rot_' + str(self._direction).zfill(2) + '_' + self._action + '-'
            context.scene.render.filepath = '/tmp/tmpfile/enemy_rot_' + str(self._direction).zfill(2) + '_' + self._action + '_' + str(round(self._frameN)).zfill(2)
            #print("first frame, setting up paths:")
            #print("target_basename: %s" % (fdts.target_basename))
            #print("render filepath: %s" % (context.scene.render.filepath))

            print("")

            print("\033[1;37mRendering %s (%s) %s/%s ...\033[0;37m" % (self._action, self._direction, int(self._frameN), int(self._frameEnd)))

            fdts.status = "Rendering %s (%s) %s/%s ..." % (self._action, self._direction, int(self._frameN), int(self._frameEnd))

            #time.sleep(0.1)
            context.scene.frame_current = self._frameN
            bpy.ops.render.render(write_still=True)
            self._frameN += 1

            print("\033[1;32mRender done!\033[0;37m")

            if self._frameN == self._frameEnd:
                do_cam_rot()
                self._direction += 1
                self._frameN = self._frameStart
                if self._direction == fdts.number_of_directions:
                    self._actionN += 1
                    if self._actionN == len(self.renderList):
                        print("finished")
                        return self.cancel(context)
                    else:
                        self.loadAction(self._actionN)

            if self._frameN > self._frameEnd:
                print("\033[1;31mSomething went horribly wrong!!\033[0;37m")
                return self.cancel(context)

        return {'PASS_THROUGH'}

    def execute(self, context):
        self.renderList = []
        for action in bpy.data.actions:
            if action.render == True:
                print("Appended action for rendering: %s" % (action.name))
                self.renderList.append(action)

        _actionN = 0
        self.loadAction(0)

        fdts = context.scene.freedroid_tools_settings
        #arm = bpy.data.objects[fdts.target_armature]
        #act = bpy.data.actions.get(self.action)


        #fdts.status = "rendering " + self.action

        if self._action:

            #loadAction()

            #self._frameStart = act.frame_range.x
            #self._frameEnd = act.frame_range.y
            #self._frameN = self._frameStart


            self._rendering = True

            fdts.status = "Rendering %s %s/%s ..." % (self._action, int(self._frameN), int(self._frameEnd))

            reset_cam()

            wm = context.window_manager
            self._timer = wm.event_timer_add(0.1, context.window)
            wm.modal_handler_add(self)

            return {'RUNNING_MODAL'}
        else:
            return {'CANCELLED'}

    def cancel(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)
        print("cancelled!")
        context.scene.freedroid_tools_settings.status = "Render droid"
        return {'CANCELLED'}


class fdt_renderDroid(Operator):
    bl_idname = "fdt.render_droid"
    bl_label = "Render droid"
    bl_description = "Render droid"

    def execute(self, context):
        bpy.ops.fdt.render_action()

        #info("FreedroidTools*Rendering stand cycle")
        #render_action("stand")
        #info("FreedroidTools*Rendering walk cycle")
        #render_action("walk")
        #info("FreedroidTools*Rendering attack cycle")
        #render_action("attack")
        #info("FreedroidTools*Rendering gethit cycle")
        #render_action("gethit")
        #info("FreedroidTools*Rendering death cycle")
        #render_action("death")
        #info("FreedroidTools*Rendering done")
        return {'FINISHED'}


#
# little info dialog popup because blender kind of lacks this still
#
class infoDialog(bpy.types.Operator):
    bl_idname = "wm.info_dialog"
    bl_label = ""

    msg = bpy.props.StringProperty(name = "msg")

    def execute(self, context):
        self.report({'INFO'}, self.msg)
        return {'FINISHED'}

    def draw(self, context):
        layout = self.layout

        i = 0
        msg_lines = self.msg.split('*')
        for line in msg_lines:
            row = layout.row()
            if (i == 0):
                row.label(text=line, icon="ERROR")
            else:
                row.label(text=line)
            i += 1

        row = layout.row()
        row.label(text = "")

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

#
# and for convenience a bit quicker way to use the above
def info(s):
    bpy.ops.wm.info_dialog('INVOKE_DEFAULT', msg=s)

# Define "Extras" menu
def menu_func(self, context):
    self.layout.separator()
    #self.layout.menu("INFO_MT_mesh_vert_add", text="Single Vert", icon="LAYER_ACTIVE")
    self.layout.operator("fdt.create_iso_lamps", text="Iso Lamps", icon="PLUGIN")

    self.layout.separator()




#
#--------------------------------------------------------------------------------
# registration and things
#--------------------------------------------------------------------------------
#


def register():
    bpy.utils.register_class(freedroidToolsPanel)
    bpy.utils.register_class(fdt_getTargetFromOutput)
    bpy.utils.register_class(fdt_renderDroid)
    bpy.utils.register_class(fdt_renderAction)
    bpy.utils.register_class(freedroidToolsSettings)
    bpy.utils.register_class(infoDialog)
    bpy.utils.register_class(fdt_setIsoRenderSettings)
    bpy.utils.register_class(fdt_createIsoCam)
    bpy.utils.register_class(fdt_createRotoCam)
    bpy.utils.register_class(fdt_createIsoLamps)
    bpy.utils.register_class(fdt_createIsoLampsClassic)
    bpy.utils.register_class(fdt_createShadowPlane)
    bpy.utils.register_class(fdt_createOffsetEmpty)

    bpy.types.Action.render = bpy.props.BoolProperty(default=False)

    #bpy.utils.register_class(freedroidToolsMenu)



    Scene.freedroid_tools_settings = PointerProperty(type=freedroidToolsSettings)
    bpy.app.handlers.render_post.append(runCroppy)

    # This holds armature names for current scene
    Scene.armatureList = bpy.props.CollectionProperty(type=bpy.types.PropertyGroup)

    bpy.app.handlers.scene_update_post.append(buildArmatureList)

    print("FreedroidTools registered")

def unregister():
    bpy.utils.unregister_class(freedroidToolsPanel)
    bpy.utils.unregister_class(fdt_getTargetFromOutput)
    bpy.utils.unregister_class(fdt_renderDroid)
    bpy.utils.unregister_class(fdt_renderAction)
    bpy.utils.unregister_class(freedroidToolsSettings)
    bpy.utils.unregister_class(infoDialog)
    bpy.utils.unregister_class(fdt_setIsoRenderSettings)
    bpy.utils.unregister_class(fdt_createIsoCam)
    bpy.utils.unregister_class(fdt_createRotoCam)
    bpy.utils.unregister_class(fdt_createIsoLamps)
    bpy.utils.unregister_class(fdt_createIsoLampsClassic)
    bpy.utils.unregister_class(fdt_createShadowPlane)
    bpy.utils.unregister_class(fdt_createOffsetEmpty)

    #bpy.utils.unregister_class(freedroidToolsMenu)

    del bpy.types.Action.render
    del Scene.freedroid_tools_settings
    del Scene.armatureList
    bpy.app.handlers.render_post.remove(runCroppy)
    bpy.app.handlers.scene_update_post.remove(buildArmatureList)
    print("FreedroidTools unregistered")


#register()
if __name__ == "__main__":
    try:
        unregister()
    except:
        pass

    register()
