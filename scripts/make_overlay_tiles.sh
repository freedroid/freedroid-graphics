#! /bin/bash
input="$1"
output="$2"
shift 2
n=1
for mask in "$@" ; do
	convert -alpha off -compose copyopacity -composite  "$input" "$mask" iso_"$output"_$(printf %04d $n).png
	n=$(( n + 1 ))
 done
