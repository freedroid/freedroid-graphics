#!/bin/bash
output="$1"
shift
convert -virtual-pixel transparent -filter Lanczos +distort affineprojection 0.7071068,0.3535534,-0.7071068,0.3535534,0,0 -resize 141x71 "$@" iso_"$output"_%04d.png
